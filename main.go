package main

import (
	"os"
	"os/exec"
	"github.com/valyala/fasthttp"
	"fserver/configuration"
	"sync"
	"fmt"
	"bytes"
	"net/http"
	"fserver/routes"
	"time"
	"log"
	"path"
	"fserver/tls"
)

var (
	domain = []byte(".")
	all = []byte("*")
	slesh = []byte("/")
	doubleDots = []byte(":")
)

var (
	NotFoundHandler = func(ctx *fasthttp.RequestCtx) {
		ctx.Error(fmt.Sprintf(`Path %s was not found on server
	©AVESoft 2018. All rights reserved.`, string(ctx.Path())), http.StatusNotFound)
	}
)

type Domain struct {
	Main        *fasthttp.FS
	SubDomains  map[string]*fasthttp.FS
}

type Serving struct {
	Info string
	Path string
	Selector func(*fasthttp.RequestCtx) bool
	handler  fasthttp.RequestHandler
}

func (s *Serving) Invoke(ctx *fasthttp.RequestCtx) {
	s.handler(ctx)

	if code := ctx.Response.StatusCode(); code >= 400 {
		go fmt.Printf(`{ "domain": "%s", "stamp": "%s", "path": "%s", "code": "%d", "error": "%s" }` + "\n",
			s.Info,
			ctx.Time().Format(time.ANSIC),
			ctx.Path(),
			ctx.Response.StatusCode(),
			ctx.Response.Body(),
		)
	} else {
		go fmt.Printf(`{ "domain": "%s", "stamp": "%s", "path": "%s", "code": "%d" }` + "\n",
			s.Info,
			ctx.Time().Format(time.ANSIC),
			ctx.Path(),
			ctx.Response.StatusCode(),
		)
	}
}

var (
	servings []*Serving
	overrides []*Serving
	defaultServing *Serving
)

var (
	baseConfig = func() string {
		return path.Join(configuration.BaseBinPath, os.Getenv("CONFIG_FILE"))
	}()
)

func main() {
	wg := &sync.WaitGroup{}

	var file string
	if len(os.Args) > 1 {
		file = os.Args[1]
	} else {
		file = baseConfig
	}

	config := configuration.Parse(file)
	if err := configuration.Validate(config); err != nil {
		log.Fatal(err)
	}
	domain = append(domain, []byte(config.Domain)...)

	for k, v := range config.Sub {
		createServing(config, k, v, func(serving *Serving) {
			if k == "*" {
				defaultServing = serving
			} else {
				servings = append(servings, serving)
			}
		})
	}

	for _, v := range config.Override {
		v.BValue = []byte(v.Value)
		overrides = append(overrides, generateOverride(v))
	}

	wg.Add(len(config.Ports))

	var hosts []string
	for k := range config.Sub {
		hosts = append(hosts, k + "." + config.Domain)
	}
	renewService := tls.NewCertificatesService(hosts...)


	for _, p := range config.Ports {
		go func(port *configuration.Port) {
			defer wg.Done()

			var handler fasthttp.RequestHandler
			if port.Proxy != "" {
				handler = Proxy(port)
			} else {
				handler = requestHandler
			}
			port.Value = fmt.Sprintf(":%s", port.Value)

			if port.Tls != nil {
				fmt.Printf(`{ "start": true, "port": "%s", time: "%s", "isTls": true }` + "\n",
					port.Value,
					time.Now().Format(time.ANSIC),
				)
				if err := fasthttp.ListenAndServeTLS(port.Value,
					configuration.Path(port.Tls.CertFile),
					configuration.Path(port.Tls.KeyFile),
				handler); err != nil {
					log.Fatalf("Error in TLS server(%s): %s", port.Value, err)
				}
			} else if port.AutoSSL {
				fmt.Printf(`{ "start": true, "port": "%s", time: "%s", "autoSSL": true }` + "\n",
					port.Value,
					time.Now().Format(time.ANSIC),
				)

				if err := fasthttp.Serve(renewService.Listener(port.Value), handler); err != nil {
					log.Fatalf("Error in autoSSL server(%s): %s", port.Value, err)
				}
			} else {
				fmt.Printf(`{ "start": true, "port": "%s", time: "%s", "isTls": false }` + "\n",
					port.Value,
					time.Now().Format(time.ANSIC),
				)
				if err := fasthttp.ListenAndServe(port.Value, handler); err != nil {
					log.Fatalf("Error in server(%s): %s", port.Value, err)
				}
			}
		}(p)
	}
	wg.Wait()
}

func createServing(config *configuration.Configuration, key string, value interface{}, rt func(*Serving)) {
	subdomain := []byte(key)

	config.ParseSub(value, func(path string) {
		rt(&Serving{
			Info: key,
			Path: path,
			Selector: func(ctx *fasthttp.RequestCtx) bool {
				return bytes.Equal(bytes.TrimSuffix(ctx.Host(), domain), subdomain)
			},
			handler: fasthttp.FSHandler(configuration.Path(path), 0),
		})
	}, func(obj *configuration.SubObj) {
		var handler fasthttp.RequestHandler
		if len(obj.Redirect) == 0 {
			handler = customFileHandler(obj)
		} else {
			var fh fasthttp.RequestHandler
			if len(obj.Path) > 0 {
				fh = customFileHandler(obj)
			} else {
				fh = NotFoundHandler
			}

			handler = func(ctx *fasthttp.RequestCtx) {
				for k, v := range obj.Redirect {
					fmt.Println(k, "==", string(ctx.Path()))
					if string(ctx.Path()) == k {
						ctx.Redirect(v, http.StatusFound)
						return
					}
				}
				fh(ctx)
			}
		}

		rt(&Serving{
			Info: key,
			Path: obj.Path,
			Selector: func(ctx *fasthttp.RequestCtx) bool {
				host := ctx.Host()
				if dd := bytes.Index(host, doubleDots); dd >= 0 {
					host = host[0:dd]
				}
				return bytes.Equal(bytes.TrimSuffix(host, domain), subdomain)
			},
			handler: handler,
		})
	})
}

func requestHandler(ctx *fasthttp.RequestCtx) {
	for _, s := range overrides {
		if s.Selector(ctx) {
			s.Invoke(ctx)
			return
		}
	}

	selected := false
	for _, s := range servings {
		if s.Selector(ctx) {
			s.Invoke(ctx)
			selected = true
			break
		}
	}
	if !selected {
		defaultServing.Invoke(ctx)
	}
}

func customFileHandler(obj *configuration.SubObj) fasthttp.RequestHandler {
	var handler fasthttp.RequestHandler
	if obj.OnePage {
		handler = routes.OnepageFSHandler(obj.Path)
	} else {
		handler = fasthttp.FSHandler(obj.Path, 0)
	}

	if obj.SecureOnly {
		return func(ctx *fasthttp.RequestCtx) {
			if !ctx.IsTLS() {
				ctx.Redirect(fmt.Sprintf("https://" + string(ctx.Host()) + string(ctx.RequestURI())), http.StatusTemporaryRedirect)
			} else {
				handler(ctx)
			}
		}
	}

	return handler
}

func generateOverride(over *configuration.Overriding) *Serving {
	comparing := generateComparing(over)
	var handler fasthttp.RequestHandler
	if len(over.Directory) > 0 {
		handler = fasthttp.FSHandler(configuration.Path(over.Directory), bytes.Count(over.BValue, slesh) - 1)
	} else if len(over.Redirect) > 0 {
		handler = func(ctx *fasthttp.RequestCtx) {
			ctx.Redirect(over.Redirect, http.StatusFound)
		}
	}

	if over.Type == "path" {
		return &Serving{
			Selector: func(ctx *fasthttp.RequestCtx) bool {
				return comparing(ctx.Path())
			},
			handler: handler,
		}
	} else if over.Type == "domain" {
		return &Serving{
			Selector: func(ctx *fasthttp.RequestCtx) bool {
				return comparing(ctx.Host())
			},
			handler: handler,
		}
	}
	panic("Not implemented: " + over.Type)
}

func generateComparing(over *configuration.Overriding) func([]byte) bool  {
	base := func(compare func([]byte, []byte) bool) func([]byte) bool {
		return func(s []byte) bool {
			return compare(s, over.BValue)
		}
	}

	if over.Comparing == "prefix" {
		return base(bytes.HasPrefix)
	} else if over.Comparing == "equal" {
		return base(bytes.EqualFold)
	} else if  over.Comparing == "suffix" {
		return base(bytes.HasSuffix)
	}
	panic("Override must have one of comparators: prefix, equal, suffix")
}

func Proxy(port *configuration.Port) fasthttp.RequestHandler {
	proxyClient := &fasthttp.HostClient{
		Addr: port.Proxy,
	}
	return func(ctx *fasthttp.RequestCtx) {
		req := &ctx.Request
		resp := &ctx.Response

		// do not proxy "Connection" header
		req.Header.Del("Connection")

		if err := proxyClient.Do(req, resp); err != nil {
			ctx.Logger().Printf("error when proxying the request: %s", err)
		}

		// do not proxy "Connection" header
		resp.Header.Del("Connection")
	}
}

func update(name string) chan string {
	c := make(chan string)
	go func() {
		if err := exec.Command("service", name, "restart").Run(); err != nil {
			c <- err.Error()
		} else {
			c <- "OK"
		}
	}()
	return c
}