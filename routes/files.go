package routes

import (
	"strings"
	"path"
	"github.com/valyala/fasthttp"
	"fserver/configuration"
)

func OnepageFSHandler(root string) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		p := string(ctx.Path())
		ls := strings.Split(p, "/")
		if !strings.Contains(ls[len(ls) - 1], ".") {
			p = "index.html"
		}
		fasthttp.ServeFile(ctx, configuration.Path(path.Join(root, "/"+p)))
	}
}