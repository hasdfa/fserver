package configuration

import (
	"io/ioutil"
	"encoding/json"
	"reflect"
	"fmt"
	"strings"
	"path"
	"os"
	"google.golang.org/appengine"
)

var (
	BaseBinPath = func() string {
		gopath := os.Getenv("GOPATH")
		if gopath == "" {
			if appengine.IsAppEngine() {
				return "/srv/gopath/src/fserver/"
			} else {
				return "/gopath/src/"
			}
		}
		return path.Join(gopath, "src/fserver/")
	}()
)

type Configuration struct {
	Domain   string    `json:"domain"`
	Ports    []*Port   `json:"ports"`
	Sub      map[string]interface{} `json:"sub"`
	Override []*Overriding `json:"override"`
	Updater  []*Updater `json:"updater"`
}

type Overriding struct {
	Type       string   `json:"type"`
	Directory  string   `json:"directory"`
	Redirect   string   `json:"redirect"`

	Comparing  string   `json:"comparing"`
	Value      string   `json:"value"`
	BValue     []byte
}

type Port struct {
	Value   string  `json:"value"`
	Tls     *Tls    `json:"tls"`
	AutoSSL bool    `json:"autoSSL"`
	Proxy   string  `json:"proxy"`
}

type Tls struct {
	CertFile  string `json:"certFile"`
	KeyFile   string `json:"keyFile"`
}

type Updater struct {
	Destination string `json:"dest"`
	Method      string `json:"method"`
	Path        string `json:"path"`
	Key         struct {
		Name  string `json:"name"`
		Value string `json:"value"`
	} `json:"key"`
	Script     string `json:"script"`
}

type SubObj struct {
	Path       string `json:"path"`
	SecureOnly bool   `json:"secureOnly"`
	OnePage    bool   `json:"onePage"`


	Redirect   map[string]string `json:"redirect"`
}

var configuraion *Configuration

func Instance() *Configuration {
	return configuraion
}

func Path(p string) string {
	return path.Clean(privatePath(p))
}

func privatePath(path string) string {
	if strings.Contains(path, "${bin}") {
		return strings.Replace(path, "${bin}", BaseBinPath, 1)
	}
	return path
}

func (c *Configuration) ParseSub(sub interface{}, path func(string), obj func(*SubObj)) {
	if p, ok := sub.(string); ok {
		path(p)
	} else if mp, ok := sub.(map[string]interface{}); ok {
		secureOnly := false
		if v, ok := mp["secureOnly"].(bool); ok {
			secureOnly = v
		}

		page := false
		if v, ok := mp["onePage"].(bool); ok {
			page = v
		}

		path := ""
		if v, ok := mp["path"].(string); ok {
			path = v
		}

		redirect := map[string]string{}
		if m, ok := mp["redirect"].(map[string]interface{}); ok {
			for k, v := range m {
				if s, ok := v.(string); ok {
					redirect[k] = s
				}
			}
		}

		obj(&SubObj{
			Path: Path(path),
			SecureOnly: secureOnly,
			OnePage: page,
			Redirect: redirect,
		})
	} else {
		panic("Unknown type: " + reflect.TypeOf(sub).Name() + "; " + fmt.Sprint(sub))
	}
}

func Parse(filename string) *Configuration {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("Wrong configuration file: " + err.Error())
	}
	var c Configuration
	err = json.Unmarshal(bytes, &c)
	if err != nil {
		panic("Could not parse json: " + err.Error())
	}
	configuraion = &c
	return Instance()
}

func Validate(config *Configuration) error {
	return nil
}