package scripts

import (
	"os"
	"strings"
	"flag"
	"path/filepath"
	"log"
)

func main() {
	flag.Parse()
	err := filepath.Walk("./", visit)
	for ; IsFileExists("./node_modules") || IsFileExists("./src"); {
		err = filepath.Walk("./", visit) // remove empty folders
	}
	log.Println("Walked: ", err)
}

func visit(path string, f os.FileInfo, err error) error {
	if path != "./" && !strings.HasPrefix(path, "/dist") && !strings.Contains(path, "/.git") {
		os.Remove(path)
	}
	return nil
}

func IsFileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		return !os.IsNotExist(err)
	}
	return true
}