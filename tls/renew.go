package tls

import (
	"golang.org/x/crypto/acme/autocert"
	"crypto/tls"
	"net"
)

type (
	SSLCertificatesService interface {
		Certificate() getCertificateFunc
		TlsConfig() *tls.Config
		Listener(string) net.Listener
	}

	renewService struct {
		manager *autocert.Manager
	}

	getCertificateFunc func (*tls.ClientHelloInfo) (*tls.Certificate, error)
)

func NewCertificatesService(hosts ...string) SSLCertificatesService {
	return &renewService{
		manager: &autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(hosts...),
			Cache:      autocert.DirCache("fserver-ssl"),
		},
	}
}

// Listener listens on the standard TLS port (443) on all interfaces
// and returns a net.Listener returning *tls.Conn connections.
//
// The returned listener uses a *tls.Config that enables HTTP/2, and
// should only be used with servers that support HTTP/2.
//
// The returned Listener also enables TCP keep-alives on the accepted
// connections. The returned *tls.Conn are returned before their TLS
// handshake has completed.
//
// Unlike NewListener, it is the caller's responsibility to initialize
// the Manager m's Prompt, Cache, HostPolicy, and other desired options.
func (s *renewService) Listener(port string) net.Listener {
	ln := &listener{
		m: s.manager,
		conf: &tls.Config{
			GetCertificate: s.manager.GetCertificate,  // bonus: panic on nil m
			NextProtos:     []string{"h2", "http/1.1"}, // Enable HTTP/2
		},
	}
	ln.tcpListener, ln.tcpListenErr = net.Listen("tcp", port)
	return ln
}

func (s *renewService) Certificate() getCertificateFunc {
	s.manager.Listener()
	return s.manager.GetCertificate
}

func (s *renewService) TlsConfig() *tls.Config {
	return &tls.Config{
		GetCertificate: s.Certificate(),
		NextProtos:     []string{"h2", "http/1.1"}, // Enable HTTP/2
	}
}